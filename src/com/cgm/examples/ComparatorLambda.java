package com.cgm.examples;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

@SuppressWarnings("ComparatorCombinators")
public class ComparatorLambda {

    public static void main(String[] args) {
        // anonymous inner class object
//        Comparator<String> comparator = new Comparator<String>() {
//            @Override
//            public int compare(String s1, String s2) {
//                return Integer.compare(s1.length(), s2.length());
//            }
//        };

        // example with lambda
        Comparator<String> lambdaComparator = (s1, s2) -> Integer.compare(s1.length(), s2.length());

        List<String> list = Arrays.asList("***", "**", "****", "*", "*****");
        list.sort(lambdaComparator);
        list.forEach(System.out::println);

        print("comparator");

        // lambda with streams
        List<String> personsList = Arrays.asList("Alex", "Claudiu", "Bogdan", "Rares", "Adi");
        personsList
                .stream()
                .filter( person -> person.startsWith("A"))
                .sorted()
                .forEach(System.out::println);

        print("filter");

        // lambda with streams
        Stream.of("d2", "a2", "b1", "b3", "c")
                .filter(s -> {
                    System.out.println("filter: " + s);
                    return true;
                })
                .forEach(s -> System.out.println("forEach: " + s));

        print("filter and forEach");

        // lambda with streams
        Stream.of("d2", "a2", "b1", "b3", "c")
                .filter(s -> {
                    System.out.println("filter: " + s);
                    return s.startsWith("a");
                })
                .map(s -> {
                    System.out.println("map: " + s);
                    return s.toUpperCase();
                })
                .forEach(s -> System.out.println("forEach: " + s));

        print("filter, map and forEach");
    }

    private static void print(String comparator) {
        System.out.println("----------------" + comparator + "\n");
    }
}


