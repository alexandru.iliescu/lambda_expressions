package com.cgm.examples;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class ConsumerLambda {

    public static void main(String[] args) {
        List<String> clients = new ArrayList<>();
        clients.add("first client");
        clients.add("second client");
        clients.add("third client");

        clients.forEach(client -> System.out.println(client));
        clients.forEach(System.out::println);

        Function<Process, Boolean> lambda = process -> process.isAlive();
        Function<Process, Boolean> methodReference = Process::isAlive;

    }
}
