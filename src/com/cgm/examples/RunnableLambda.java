package com.cgm.examples;

public class RunnableLambda {


    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName() + ": RunnableTest");

        // anonymous Runnable
        Runnable task1 = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " is running");
            }
        };

        // lambda expression
        Runnable task2 = () -> System.out.println(Thread.currentThread().getName() + " is running");

        new Thread(task1).start();
        new Thread(task2).start();
    }
}







