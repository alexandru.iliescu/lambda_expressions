package com.cgm.examples;

public class ThreadLambda {

    public static void main(String[] args) {
        // anonymous class inside Thread constructor
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("New thread created.");
//            }
//        }).start();

        // lambda expression
        new Thread(() -> System.out.println("New thread created.")).start();
    }
}


