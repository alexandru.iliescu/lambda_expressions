package com.cgm.main;

import java.io.File;
import java.io.FileFilter;

public class Main {

    public static void main(String[] args) {

        // anonymous inner class object
//        FileFilter filter = new FileFilter() {
//            @Override
//            public boolean accept(File pathname) {
//                return pathname.getName().endsWith(".java");
//            }
//        };

        FileFilter lambdaFilter = pathname -> pathname.getName().endsWith(".java");

        File dir = new File("d:/tmp");
        File[] files = dir.listFiles(lambdaFilter);

        for (File f : files) {
            System.out.println(f);
        }
    }
}
